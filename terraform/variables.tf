variable "ami_id" {
  type        = string
  default     = "ami-0a91cd140a1fc148a"
}

variable "key_name" {
  type        = string
  default     = "terraform-puppet"
}

variable "private_key_path" {
  type        = string
  default     = "~/.ssh/private_key.pem" 
}
